<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SignUpController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/member/login', [MemberController::class, 'login']);
Route::get('/member/register', [MemberController::class, 'register']);
Route::get('/member', [MemberController::class, 'index']);
Route::get('/member/profile', [MemberController::class, 'profile']);



//simple way of setting a new router
// Route::get('/kelas', function () {
//     return "<h1>Virtual Bootcamp Laravel</h1>";
// });

//Router using view
Route::get('/kelas', function () {
    return view('kelas');
});

Route::get('dashboard', [
    MemberController::class,
    'index'
])->name('dashboard')->middleware(['auth']);

// Route::view('dashboard', 'dashboard')
// 	->name('dashboard')
// 	->middleware(['auth', 'verified']);


// -- Admin Users Module

// --- GET /admin/user/x
// --- POST /admin/user -- insert new
// --- DELETE /admin/user/x -- delete
// --- GET /admin/user --untuk senarai

Route::middleware(['auth', 'can:is-admin'])->group(function () {

    Route::resource('admin/user', UserController::class);
    Route::resource('admin/role', RoleController::class);
    Route::resource('admin/plan', PlanController::class);
});

//signup page
Route::get('/signup', [SignUpController::class, 'index']);
