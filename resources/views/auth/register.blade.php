@extends('layout.public')

@section('content')
<div class="text-center">
    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
</div>
@if ($errors->any())
<div>
    <div>{{ __('Whoops! Something went wrong.') }}</div>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form class="user" method="POST" action="{{ route('register') }}">
    @csrf

    <div class="form-group row">
        <div class="col-sm-12 mb-6 mb-sm-0">
            <input type="text" name="name" class="form-control form-control-user" id="exampleFirstName" value="{{ old('name') }}" required autofocus autocomplete="name" placeholder="{{ __('Name') }}">
        </div>
    </div>

    <div class="form-group">
        <input type="email" class="form-control form-control-user" id="exampleInputEmail" name="email" value="{{ old('email') }}" required placeholder="{{ __('Email') }}">
    </div>

    <div class="form-group row">
        <div class="col-sm-6 mb-3 mb-sm-0">
            <input type="password" name="password" required autocomplete="new-password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ __('Password') }}">
        </div>
        <div class="col-sm-6">
            <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
        </div>
    </div>

    <div class="text-center">
        <a class="small" href="{{ route('login') }}">{{ __('Already registered?') }}</a>
    </div>


    <div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            {{ __('Register') }}
        </button>
    </div>
</form>
@endsection