@extends('layout.public')

@section('content')
<div class="text-center">
    <h1 class="h4 text-gray-900 mb-4">Welcome Back! Please Login</h1>
</div>
@if (session('status'))
<div>
    {{ session('status') }}
</div>
@endif

@if ($errors->any())
<div>
    <div>{{ __('Whoops! Something went wrong.') }}</div>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form class="user" method="POST" action="{{ route('login') }}">
    @csrf

    <div class="form-group">
        <input type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" required autofocus aria-describedby="emailHelp" placeholder="{{ __('Email') }}">
    </div>

    <div class="form-group">
        <input type="password" class="form-control form-control-user" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
    </div>

    <div class="form-group">
        <div class="custom-control custom-checkbox small">
            <input type="checkbox" class="custom-control-input" id="customCheck" name="remember">
            <label class="custom-control-label" for="customCheck">{{ __('Remember me') }}</label>
        </div>
    </div>

    @if (Route::has('password.request'))
    <div class="text-center">
        <a class="small" href="{{ route('password.request') }}">{{ __('Forgot your password?') }}</a>
    </div>
    @endif

    <div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            {{ __('Login') }}
        </button>
    </div>
</form>
@endsection