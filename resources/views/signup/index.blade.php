@extends('layout.public')

@section('content')

<h2>Pendaftaran</h2>
<p>Sila Pilih Plan</p>

<div class="row">
    @foreach($plans as $plan)
    <div class="col">
        <div class="card shadow">
            <div class="card-body">
                <h4>{{ $plan->name }}</h4>
                <h5>RM {{ number_format($plan->price / 100, 2, '.') }}</h5>
                <p class="mb-5"><strong>Tempoh: {{ $plan->duration }} hari.</strong></p>

                <button data-plan-id="{{$plan->id}}" class="plan-button btn btn-warning" data-toggle="modal" data-target="#loginOrRegister-modal">Langgan Sekarang!</button>
            </div>
        </div>
    </div>
    @endforeach
</div>


<!--Modal-->
<div class="modal fade" id="loginOrRegister-modal" tabindex="1" aria-labelledby="loginOrRegisterLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginOrRegisterLabel">Sila Login atau Daftar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Sila login atau daftar sebelum membuat bayaran.</p>

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="login-tab" data-toggle="tab" data-target="#login" type="button" role="tab" aria-controls="home" aria-selected="true">Login</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="register-tab" data-toggle="tab" data-target="#register" type="button" role="tab" aria-controls="register" aria-selected="false">Register</button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">

                        <form class="user p-5" id="login-form-form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="alert alert-danger d-none" id="login-alert">
                                Email atau katalaluan tidak sah.
                            </div>
                            <div class="form-group">
                                <input id="login-form-email" type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" required autofocus aria-describedby="emailHelp" placeholder="{{ __('Email') }}">
                            </div>

                            <div class="form-group">
                                <input id="login-form-password" type="password" class="form-control form-control-user" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
                            </div>

                            @if (Route::has('password.request'))
                            <div class="text-center">
                                <a class="small" href="{{ route('password.request') }}">{{ __('Forgot your password?') }}</a>
                            </div>
                            @endif

                            <div>
                                <button type="button" id="login-button" class="btn btn-primary btn-user btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">

                        <form class="user p-5" method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="form-group row">
                                <div class="col-sm-12 mb-6 mb-sm-0">
                                    <input type="text" name="name" class="form-control form-control-user" id="exampleFirstName" value="{{ old('name') }}" required autofocus autocomplete="name" placeholder="{{ __('Name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control form-control-user" id="exampleInputEmail" name="email" value="{{ old('email') }}" required placeholder="{{ __('Email') }}">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" name="password" required autocomplete="new-password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ __('Password') }}">
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                                </div>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection

@section('page-js')
<script src="/js/signup.js"></script>
@endsection