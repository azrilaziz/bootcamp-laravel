@extends('layout.private')

@section('content')
<!-- Page Heading -->
<div class="d-flex justify-content-between mb-3">
    <h1 class="h3 mb-2 text-gray-800">Senarai Peranan Pengguna</h1>
    <a href="/admin/role/create" class="btn btn-secondary">Tambah Peranan</a>
</div>

@if(session('success'))
<div class="alert alert-success">{!! session('success') !!}</div>
@endif

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Senarai Peranan</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <!-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"> -->

            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAMA</th>
                        <th>&nbsp;</th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>NAMA</th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <form action="{{route('role.destroy', $role->id)}}" method="POST" onsubmit="return confirm('Are you sure to delete role {{$role->name}}?');" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger">DELETE</button>
                            </form>
                            <a href="{{route('role.edit', $role->id)}}" class="btn btn-sm btn-primary">EDIT</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/template/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="/template/js/demo/datatables-demo.js"></script>

@endsection