@extends('layout.private')

@section('content')
<!-- Page Heading -->
<div class="d-flex justify-content-between mb-3">
    <h1 class="h3 mb-2 text-gray-800">Senarai Pelan Pengguna</h1>
    <a href="/admin/plan/create" class="btn btn-secondary">Tambah Pelan</a>
</div>

@if(session('success'))
<div class="alert alert-success">{!! session('success') !!}</div>
@endif

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Senarai Pelan</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <!-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"> -->

            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAMA</th>
                        <th>HARGA</th>
                        <th>TEMPOH</th>
                        <th>&nbsp;</th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>NAMA</th>
                        <th>HARGA</th>
                        <th>TEMPOH</th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
                <tbody>

                    @forelse($plans as $plan)
                    <tr>
                        <td>{{ $plan->id }}</td>
                        <td>{{ $plan->name }}</td>
                        <td>RM {{ number_format($plan->price / 100, 2, '.') }}</td>
                        <td>{{ $plan->duration }}</td>
                        <td>
                            <form action="{{route('plan.destroy', $plan->id)}}" method="POST" onsubmit="return confirm('Are you sure to delete plan {{$plan->name}}?');" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger">DELETE</button>
                            </form>
                            <a href="{{route('plan.edit', $plan->id)}}" class="btn btn-sm btn-primary">EDIT</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" class="text-center" style="height:50px;">
                            Anda belum membuat apa-apa pelan langganan...
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/template/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="/template/js/demo/datatables-demo.js"></script>

@endsection