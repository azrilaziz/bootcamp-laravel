@extends('layout.private')

@section('content')
@if (session('status'))

@endif

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Edit Pengguna</h1>


<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ $user->name }}</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('user.update', $user->id) }}" method="POST">
            @csrf
            @method('put')

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror()" name="name" value="{{ old('name', $user->name) }}">
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror()

                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('email') is-invalid @enderror()" name="email" value="{{ old('email', $user->email) }}">
                    @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror()
                </div>
            </div>

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Peranan</label>
                <div class="pl-1 custom-control checkbox mr-5">
                    @foreach($roles as $role)
                    <label class="col-sm-8">
                        <input @if(in_array($role->name, $user->roles()->pluck('name')->toArray() ))
                        checked
                        @endif
                        type="checkbox" value="{{$role->id}}" name="roles[]">
                        {{$role->name}}
                    </label>
                    @endforeach
                </div>
            </div>

            <button class="btn btn-lg btn-primary">Simpan</button>

        </form>
    </div>
</div>

@endsection