<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    // Member
    public function index()
    {
        return view('dashboard');
    }

    //member profile
    public function profile()
    {
        return view('member.profile');
    }

    //Member Login
    public function login()
    {
        return view('member.login');
    }

    //Member Register
    public function register()
    {
        return view('member.register');
    }
}
